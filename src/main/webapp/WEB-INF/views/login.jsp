<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
<title>Add new user</title>
</head>
<body>
<div class="container">
<h2>Add new user</h2>
	<form action="home" method="post">
	    <label>Name:</label>
		<input type="text" name="userName"><br>
	    <label>Phone:</label>
		<input type="text" name="userPhone"><br>
		<input type="submit" value="Add">
	</form>
</div>
</body>
</html>